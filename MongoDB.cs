﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proiect_mongo_db
{
    public class MongoDB
    {
        public static MongoClient client = new MongoClient(
            "mongodb://127.0.0.1:27017/?compressors=disabled&gssapiServiceName=mongodb"
            );
        public static IMongoDatabase database = client.GetDatabase("test");
    }
}
