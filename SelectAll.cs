﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proiect_mongo_db
{
    public partial class SelectAll : Form
    {
        IMongoDatabase db;
        public SelectAll()
        {
            InitializeComponent();
        }

        private void SelectAll_Load(object sender, EventArgs e)
        {
            db = MongoDB.database;
            var data = db.GetCollection<BsonDocument>("students");
            //var filter = Builders<BsonDocument>.;

            var columns = data.Find(new BsonDocument()).FirstOrDefault();
            var doc = data.Find(new BsonDocument()).ToList();

            foreach( var column in columns)
            {
                if (column.Name == "address")
                {
                    foreach (var cell in column.Value.ToBsonDocument())
                    {
                        dataGridView1.Columns.Add(cell.Name, cell.Name);
                    }

                }
                else
                {
                    dataGridView1.Columns.Add(column.Name,column.Name);
                }
            }

            int i = 0;
            foreach (var row in doc)
            {
                dataGridView1.Rows.Add();
                
                foreach (var value in row)
                {
                    if(value.Name == "address")
                    {
                        foreach (var cell in value.Value.ToBsonDocument())
                        {
                            dataGridView1.Rows[i].Cells[cell.Name].Value = cell.Value.ToString();
                        }

                    }
                    else
                    {
                        dataGridView1.Rows[i].Cells[value.Name].Value = value.Value.ToString();
                    }
                    
                }
                i++;
            }
            
        }
    }
}
