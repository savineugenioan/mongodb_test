﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proiect_mongo_db
{
    public partial class Delete : Form
    {
        private IMongoDatabase db;

        public Delete()
        {
            InitializeComponent();
        }

        private void Delete_Load(object sender, EventArgs e)
        {
            db = MongoDB.database;
            var data = db.GetCollection<BsonDocument>("students");
            //var filter = Builders<BsonDocument>.;

            var columns = data.Find(new BsonDocument()).FirstOrDefault();
            var doc = data.Find(new BsonDocument()).ToList();

            foreach (var column in columns)
            {
                if (column.Name == "address")
                {
                    foreach (var cell in column.Value.ToBsonDocument())
                    {
                        dataGridView1.Columns.Add(cell.Name, cell.Name);
                    }

                }
                else
                {
                    dataGridView1.Columns.Add(column.Name, column.Name);
                }
            }

            int i = 0;
            foreach (var row in doc)
            {
                dataGridView1.Rows.Add();
                foreach (var value in row)
                {
                    if (value.Name == "address")
                    {
                        foreach (var cell in value.Value.ToBsonDocument())
                        {
                            dataGridView1.Rows[i].Cells[cell.Name].Value = cell.Value.ToString();
                        }

                    }
                    else
                    {
                        dataGridView1.Rows[i].Cells[value.Name].Value = value.Value.ToString();
                    }
                }
                i++;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                string id = dataGridView1.CurrentRow.Cells[0].Value.ToString();

                var data = db.GetCollection<BsonDocument>("students");

                var filter = Builders<BsonDocument>.Filter.Eq("_id",ObjectId.Parse(id));

                var x = data.DeleteOne(filter);
                MessageBox.Show("Stergere efectuata", "Bravo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                dataGridView1 = new DataGridView();
                this.Close();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "EROARE", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
