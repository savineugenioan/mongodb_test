﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proiect_mongo_db
{
    public partial class Update : Form
    {
        private IMongoDatabase db;
        bool first = true;
        public Update()
        {
            InitializeComponent();
        }

        void init()
        {
            db = MongoDB.database;
            var data = db.GetCollection<BsonDocument>("students");
            var columns = data.Find(new BsonDocument()).FirstOrDefault();
            foreach (var column in columns)
            {
                if (column.Name == "address")
                {
                    foreach (var cell in column.Value.ToBsonDocument())
                    {
                        dataGridView1.Columns.Add(cell.Name, cell.Name);
                    }

                }
                else
                {
                    dataGridView1.Columns.Add(column.Name, column.Name);
                }
                first = false;
            }
        }

        private void Update_Load(object sender, EventArgs e)
        {
            if (first)
            {
                init();
            }
            
            var data = db.GetCollection<BsonDocument>("students");
            var doc = data.Find(new BsonDocument()).ToList();

            int i = 0;
            foreach (var row in doc)
            {
                dataGridView1.Rows.Add();
                foreach (var value in row)
                {
                    if (value.Name == "address")
                    {
                        foreach (var cell in value.Value.ToBsonDocument())
                        {
                            dataGridView1.Rows[i].Cells[cell.Name].Value = cell.Value.ToString();
                        }

                    }
                    else
                    {
                        dataGridView1.Rows[i].Cells[value.Name].Value = value.Value.ToString();
                    }
                }
                i++;
            }
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            button1_Click(sender, e);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form f = new Edit_entry(dataGridView1.CurrentRow);
            f.ShowDialog();
            dataGridView1.Rows.Clear();
            Update_Load(sender, e);
        }
    }
}
