﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proiect_mongo_db
{
    public partial class Edit_entry : Form
    {
        DataGridViewRow row;
        private IMongoDatabase db;
        public Edit_entry()
        {
            InitializeComponent();
        }

        public Edit_entry(DataGridViewRow row)
        {
            this.row = row;
            InitializeComponent();
        }

        private void Edit_entry_Load(object sender, EventArgs e)
        {
            db = MongoDB.database;
            listBox1.SelectedItem = row.Cells["major"].Value.ToString();
            textBox1.Text = row.Cells["name"].Value.ToString();
            textBox2.Text = row.Cells["year"].Value.ToString();
            textBox4.Text = row.Cells["gpa"].Value.ToString();
            textBox5.Text = row.Cells["street"].Value.ToString();
            textBox6.Text = row.Cells["city"].Value.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                var filter = Builders<BsonDocument>.Filter.Eq("_id", ObjectId.Parse(row.Cells[0].Value.ToString()));
                var data = db.GetCollection<BsonDocument>("students");
                var update = Builders<BsonDocument>.Update.Set("name", textBox1.Text)
                                                          .Set("year", textBox2.Text)
                                                          .Set("major", listBox1.SelectedItem.ToString())
                                                          .Set("gpa", Int32.Parse(textBox4.Text))
                                                          .Set("address.street", textBox5.Text)
                                                          .Set("address.city", textBox6.Text);

                if (textBox1.Text.Trim() != "" && textBox2.Text.Trim() != "" && textBox6.Text.Trim() != "" &&
                    textBox4.Text.Trim() != "" && listBox1.SelectedIndex != -1 && textBox5.Text.Trim() != "")
                {
                    data.UpdateOne(filter, update);
                    MessageBox.Show("Editarea a fost executata cu succes", "Bravo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Completati toate campurile", "Eroare", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Eroare", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
