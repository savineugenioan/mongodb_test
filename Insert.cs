﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proiect_mongo_db
{
    public partial class Insert : Form
    {
        IMongoDatabase db;
        public Insert()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                var data = db.GetCollection<BsonDocument>("students");
                var doc = new BsonDocument();
                doc.Add("name", textBox1.Text);
                doc.Add("year", textBox2.Text);
                doc.Add("major", listBox1.SelectedItem.ToString());
                doc.Add("gpa", Int32.Parse(textBox4.Text));
                doc.Add("street", textBox5.Text);
                doc.Add("city", textBox6.Text);

                if(textBox1.Text.Trim()!= "" && textBox2.Text.Trim() != "" && textBox6.Text.Trim() != "" &&
                    textBox4.Text.Trim() != "" && listBox1.SelectedIndex!=-1 && textBox5.Text.Trim() != "" )
                {
                    data.InsertOne(doc);
                    MessageBox.Show("Introducerea a fost executata cu succes", "Bravo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Completati toate campurile", "Eroare", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Eroare", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void Insert_Load(object sender, EventArgs e)
        {
            db = MongoDB.database;
            listBox1.SelectedIndex = 0;
        }
    }
}
