﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MongoDB.Bson;
using MongoDB.Driver;


namespace Proiect_mongo_db
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form f = new SelectAll();
            f.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form f = new Insert();
            f.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Form f = new Delete();
            f.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Form f = new Update();
            f.Show();
        }
    }
}
